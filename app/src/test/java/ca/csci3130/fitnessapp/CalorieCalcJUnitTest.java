package ca.csci3130.fitnessapp;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import static org.junit.Assert.*;

/**
 * @version iteration 1
 * @author Amir Farrag, Owen Zhang
 *
 * The test is really trivial
 * 1. the input wouldn't be null since the field taking in the input is always a string initialed with ""
 *      testInputCorrectness()
 * 2. Check it translates the string to double
 * 3. Empty string returns -1 error code
 *      testInput_2()
 * 4. -4 out of range error identified
 *      testInput_3()
 * 5. not number error checked, and out of range checked
 *
 */
public class CalorieCalcJUnitTest {

    @BeforeClass
    public static void init() {
        System.out.println("Test begin");
    }

    @AfterClass
    public static void finished() {
        System.out.println("Tests ends");
    }


    @Test
    public void testInputCorrectness() {
        String[] sampleInput1 = {"24", "18", "123", "123"};
        double[] sampleResult1 = {24, 18, 123, 123};

        String[] sampleInput2 = {"", "", "", ""};
        double[] sampleResult2 = {-1, -1, -1, -1};
        //for(double a:CalculationFunction.CalorieCalInputTest(sampleInput2))
        // System.out.print(a+" ");
        assertArrayEquals(CalculationFunction.CalorieCalInputTest(sampleInput1, false, false), sampleResult1, .001);
        assertArrayEquals(CalculationFunction.CalorieCalInputTest(sampleInput2, false, false), sampleResult2, .001);
    }

    @Test
    public void testInput_2(){
        String [] sampleInput = {"100000000","24","12","123"};
        double[]sampleResult = {-4,24,12,123};
        assertArrayEquals(CalculationFunction.CalorieCalInputTest(sampleInput,false,false),sampleResult,0.00001);
    }
    @Test
    public void testInput_3(){
        String [] sampleInput = {"abc","25","12.133","123.2"};
        double[]sampleResult = {-2,-4,12.133,123.2};
        assertArrayEquals(CalculationFunction.CalorieCalInputTest(sampleInput,false,false),sampleResult,0.00001);
    }


}