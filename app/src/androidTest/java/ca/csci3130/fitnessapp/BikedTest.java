<<<<<<< HEAD
=======
/**
                 * Author: Kenny Power, Matthew Brousseau
                 * Iteration: 1 
                 * Comments: Espresso tests for testing functionality of the two bars 
                             Goal_Bike.java. Functions move the bar to the far left, far right and 
                             middle positions and checked to see the values returned.
                 */
>>>>>>> c4bbe512f633044802c153dcc0e14b0c2e4383c8

package ca.csci3130.fitnessapp;

import android.support.test.espresso.action.GeneralClickAction;
import android.support.test.espresso.action.GeneralLocation;
import android.support.test.espresso.action.Press;
import android.support.test.espresso.action.Tap;
import android.support.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;


import android.support.test.runner.AndroidJUnit4;

import org.junit.runner.RunWith;

import android.support.test.filters.LargeTest;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by mattrbrousseau on 27/02/18.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class BikedTest{


    @Rule
    public ActivityTestRule<Goal_Bike> mActivityRule = new ActivityTestRule<>(
            Goal_Bike.class);

    //Testing different positions of the seekbar: left middle and right and testing if correct values are returned

    //Mins
    @Test
    public void biking_min_update1() throws Exception {
        onView(withId(R.id.bike_min)).perform(new GeneralClickAction(Tap.SINGLE, GeneralLocation.CENTER_LEFT, Press.FINGER));
        onView(withId(R.id.bike_min_view)).check(matches(withText("0 Minutes")));
    }
    @Test
    public void biking_min_update2() throws Exception {
        onView(withId(R.id.bike_min)).perform(new GeneralClickAction(Tap.SINGLE, GeneralLocation.CENTER, Press.FINGER));
        onView(withId(R.id.bike_min_view)).check(matches(withText("120.0 Minutes")));
    }

    @Test
    public void biking_min_update3() throws Exception {
        onView(withId(R.id.bike_min)).perform(new GeneralClickAction(Tap.SINGLE, GeneralLocation.CENTER_RIGHT, Press.FINGER));
        onView(withId(R.id.bike_min_view)).check(matches(withText("240.0 Minutes")));
    }

    //Kilometers
    @Test
    public void biking_km_update1() throws Exception {
        onView(withId(R.id.bike_km)).perform(new GeneralClickAction(Tap.SINGLE, GeneralLocation.CENTER_LEFT, Press.FINGER));
        onView(withId(R.id.bike_km_view)).check(matches(withText("0 Kilometers")));
    }
    @Test
    public void biking_km_update2() throws Exception {
        onView(withId(R.id.bike_km)).perform(new GeneralClickAction(Tap.SINGLE, GeneralLocation.CENTER, Press.FINGER));
        onView(withId(R.id.bike_km_view)).check(matches(withText("50.0 Kilometers")));
    }
    @Test
    public void biking_km_update3() throws Exception {
        onView(withId(R.id.bike_km)).perform(new GeneralClickAction(Tap.SINGLE, GeneralLocation.CENTER_RIGHT, Press.FINGER));
        onView(withId(R.id.bike_km_view)).check(matches(withText("100.0 Kilometers")));
    }
}
