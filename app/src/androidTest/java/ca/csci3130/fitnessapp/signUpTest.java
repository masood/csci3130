package ca.csci3130.fitnessapp;

import android.support.test.espresso.Espresso;
import android.support.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;


import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by mrbeerslaw on 25/02/18.
 */

public class signUpTest {

    @Rule
    public ActivityTestRule<Log_in_Activity> mainSignUpTestRule = new ActivityTestRule<>(Log_in_Activity.class);

    private String signUpMsg  = "Let's Get Started";
    private String testEmail  = "will.stone143@gmail.com";
    private String testPass   = "Abc123!?";
    private String welcomeMsg = "Welcome To the Fitness App";
    private String logInMsg   = "Provide Your Information";

    @Before
    public void setUp() throws Exception{

    }

    @Test
    public void goToSignUp(){
        // Go to signUp screen, make sure activity change works
        Espresso.closeSoftKeyboard();
        Espresso.onView(withId(R.id.signUpScreenBtn)).perform(click());
        Espresso.onView(withId(R.id.signUpMsg)).check(matches(withText(signUpMsg)));
    }

    @Test
    public void makeAcct(){
        // Test an email and password to see if it makes it to the logInActivity in page (next activity)
        Espresso.closeSoftKeyboard();
        Espresso.onView(withId(R.id.signUpEmail)).perform(typeText(testEmail));
        Espresso.onView(withId(R.id.signUpPassword)).perform(typeText(testPass));
        Espresso.closeSoftKeyboard();
        Espresso.onView(withId(R.id.signUpBtn)).perform(click());
        Espresso.onView(withId(R.id.signInMsg)).check(matches(withText(logInMsg)));
    }
    /*
    @Test
    public void signIn(){
        Espresso.onView(withId(R.id.loginEmail)).perform(typeText(testEmail));
        Espresso.onView((withId(R.id.loginPass))).perform(typeText(testPass));
        Espresso.closeSoftKeyboard();
        Espresso.onView(withId(R.id.loginBtn));
        Espresso.onView(withId(R.id.welcomeScreen)).check(matches(withText(welcomeMsg)));
    }
    */

    @After
    public void tearDown() throws Exception {
    }



}
