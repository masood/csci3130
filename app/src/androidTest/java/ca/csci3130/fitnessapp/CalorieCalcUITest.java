package ca.csci3130.fitnessapp;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.ViewAssertion;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.runner.AndroidJUnit4;

import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;


import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.isClickable;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;


import android.support.test.rule.ActivityTestRule;

import org.junit.Test;
import org.junit.Rule;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * @version iteration 1
 * @auther Amir Farrag, Owen Zhang
 */
@RunWith(AndroidJUnit4.class)
public class CalorieCalcUITest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("ca.csci3130.fitnessapp", appContext.getPackageName());
    }

    @Rule
    public final ActivityTestRule<CalorieCalculator> main = new ActivityTestRule<>(CalorieCalculator.class);

    @Test
    public void EmptyInputTest() {

        onView(withId(R.id.inputDist)).perform(typeText(""));
        onView(withId(R.id.inputTime)).perform(typeText(""));
        onView(withId(R.id.inputHeight)).perform(typeText(" "+"a"));
        onView(withId(R.id.inputweight)).perform(typeText(""));
        onView(withId(R.id.calculate)).perform(click());
        onView(withId(R.id.display)).check(matches(withText("Distance field: please input something\nTime field: please input something\n")));
    }

    @Test
    public void standard_opt(){
        onView(withId(R.id.inputDist)).perform(typeText("1000"));
        onView(withId(R.id.inputTime)).perform(typeText("21"));
        onView(withId(R.id.inputweight)).perform(typeText("190"));
        onView(withId(R.id.inputHeight)).perform(typeText("186"));
        onView(withId(R.id.weightSwitch)).perform(click());
        onView(withId(R.id.calculate)).perform(click());
        onView(withId(R.id.display)).check(matches(withText("Total calories burned is: 5,969.15")));

    }

    @Test
    public void standard_cal(){
        onView(withId(R.id.inputDist)).perform(typeText("1000"));
        onView(withId(R.id.inputTime)).perform(typeText("0.5"));

        onView(withId(R.id.inputweight)).perform(clearText());
        onView(withId(R.id.inputweight)).perform(typeText("190"));

        onView(withId(R.id.inputHeight)).perform(clearText());
        onView(withId(R.id.inputHeight)).perform(typeText("186"));

        onView(withId(R.id.heightSwitch)).perform(click());

        onView(withId(R.id.calculate)).perform(click());
        onView(withId(R.id.display)).check(matches(withText("Total calories burned is: 99.44")));

    }
    @Test
    public void standard_cal2(){
        onView(withId(R.id.inputDist)).perform(typeText("1000"));
        onView(withId(R.id.inputTime)).perform(typeText("01"));

        onView(withId(R.id.inputweight)).perform(clearText());
        onView(withId(R.id.inputweight)).perform(typeText("190"));

        onView(withId(R.id.inputHeight)).perform(clearText());
        onView(withId(R.id.inputHeight)).perform(typeText("70"));

        //onView(withId(R.id.heightSwitch)).perform(click());

        onView(withId(R.id.calculate)).perform(click());
        onView(withId(R.id.display)).check(matches(withText("Total calories burned is: 516.22")));

    }

}
