/**
                 * Author: Kenny Power, Matthew Brousseau
                 * Iteration: 1 
                 * Comments: Espresso tests for testing functionality of the bar in 
                             Goal_swim.java. Functions move the bar to the far left, far right and 
                             middle positions and checked to see the values returned.
                 */

package ca.csci3130.fitnessapp;

import android.support.test.espresso.action.GeneralClickAction;
import android.support.test.espresso.action.GeneralLocation;
import android.support.test.espresso.action.Press;
import android.support.test.espresso.action.Tap;
import android.support.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by mattrbrousseau on 28/02/18.
 */

public class SwimTest {

    @Rule
    public ActivityTestRule<Goal_swim> mActivityRule = new ActivityTestRule<>(
            Goal_swim.class);


    //Testing different positions of the seekbar: left middle and right and testing if correct values are returned
    //mins
    @Test
    public void swim_min_update1() throws Exception {
        onView(withId(R.id.swim_bar)).perform(new GeneralClickAction(Tap.SINGLE, GeneralLocation.CENTER_LEFT, Press.FINGER));
        onView(withId(R.id.swim_mins)).check(matches(withText("0 Minutes")));
    }
    @Test
    public void swim_min_update2() throws Exception {
        onView(withId(R.id.swim_bar)).perform(new GeneralClickAction(Tap.SINGLE, GeneralLocation.CENTER, Press.FINGER));
        onView(withId(R.id.swim_mins)).check(matches(withText("60 Minutes")));
    }

    @Test
    public void swim_min_update3() throws Exception {
        onView(withId(R.id.swim_bar)).perform(new GeneralClickAction(Tap.SINGLE, GeneralLocation.CENTER_RIGHT, Press.FINGER));
        onView(withId(R.id.swim_mins)).check(matches(withText("120 Minutes")));
    }
}
