// File by Kenny Power

package ca.csci3130.fitnessapp;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

public class Goal_run extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goal_run);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        // Capture seekbars
        SeekBar seek1 = (SeekBar)findViewById(R.id.seekBar);
        seek1.setMax(340);
        SeekBar seek2 = (SeekBar) findViewById(R.id.seekBar2);
        seek2.setMax(42);

        // Add listener to update the minutes for first seekbar
        seek1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                // Update number txt
                updateMins((float) i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // Auto generated necessary interface method
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // Auto generated necessary interface method
            }
        });

        // Add listener to update the KMs for second seekbar
        seek2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                updateKM((float) i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // Auto generated necessary interface method
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // Auto generated necessary interface method
            }
        });
    }

    private void updateMins(float mins){
        // Method called to interactively update the minutes
        TextView t = (TextView)findViewById(R.id.run_mins);
        if (mins>=0.0 && mins<=340.0) {
            t.setText(mins + " Minutes");
        }
    }

    private void updateKM(float km){
        // Method called to interactively update the kilometers
        TextView t = (TextView)findViewById(R.id.run_kms);
        if (km>=0.0 && km<=42.0) {
            t.setText(km + " Kilometers");
        }
    }


}
