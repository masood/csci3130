// Class by Kenny Power

package ca.csci3130.fitnessapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class GoalChooser extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Auto generated code
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goal_chooser);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

//    @Override
    public void select(View view){
        Button b = (Button)view;
        String btxt = b.getText().toString();
        Intent intent = null;

        // Check which button is clicked, then go to page
        if (btxt.equals("Run")) {
            intent = new Intent(this, Goal_run.class);
        } else if (btxt.equals("Swim")) {
            intent = new Intent(this, Goal_swim.class);
        } else if (btxt.equals("Bike")){
            intent = new Intent(this, Goal_Bike.class);
        } else if (btxt.equals("Weights")) {
            intent = new Intent(this, Goal_weights.class);
        }
        startActivity(intent);
    }


}
