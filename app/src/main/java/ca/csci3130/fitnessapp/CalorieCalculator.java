package ca.csci3130.fitnessapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

/**
 * Created by Amir Farrag and Owen Zhang on 14/02/2018.
 */

public class CalorieCalculator extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calorie_calc_layout);
    }

    /**
     * This is the function corresponds to the calculation button
     *
     * @param (view) the Calculate View Button onClick
     * @return (void)
     */
    public void calculate(View view) {
        EditText _dist = findViewById(R.id.inputDist);
        EditText _time = findViewById(R.id.inputTime);
        EditText _weight = findViewById(R.id.inputweight);
        EditText _height = findViewById(R.id.inputHeight);

        //TODO: To check null input, null may result error
        Switch _exerciseType = findViewById(R.id.CWSwitch);
        Switch _weightunit = findViewById(R.id.weightSwitch);
        Switch _heightunit = findViewById(R.id.heightSwitch);

        TextView result = findViewById(R.id.display);

        // input data in the order of distance, time, weight, then height
        String[] input_test_for_DTWH = {_dist.getText().toString(), _time.getText().toString(), _weight.getText().toString(), _height.getText().toString()};
        double[] test_result = CalculationFunction.CalorieCalInputTest(input_test_for_DTWH, _weightunit.isChecked(), _heightunit.isChecked());

        String[] error_msg_gen = {"_",                                    // 0 message
                " please input something\n",            // -1 message, empty input
                " please input valid number value\n",   // -2 message, it should never occur since the input is not number
                "_",                                    // -3 message
                " please input a realistic value\n"     // -4 message, not feasible number

        }; // invalid
        String[] possible_head = {"Distance", "Time", "Weight", "Height"};
        boolean error = false;
        String error_message = "";

        for (int i = 0; i < test_result.length; i++) { // check is there error message
            if (test_result[i] < 0) {
                error = true;
                error_message += possible_head[i] + " field:" + error_msg_gen[(int) (-1 * test_result[i])];
            }
        }

        if (error) { // if error then return the error message
            result.setText(error_message);
            return;
        }

        // if reach here means input has no error, then input all the values
        int extype = (_exerciseType.isChecked()?0:1);
        double  burnedCal = CalculationFunction.CalorieCal(extype,test_result[1],test_result[2],test_result[3],test_result[0],null);

        String display = "Total calories burned is: "+ String.format("%,.2f",burnedCal);

        if(burnedCal > 50000 || burnedCal < 0) {
            display = "Check your information or units, the result seems unrealistic"+burnedCal;
        }

        // display
        result.setText(display);

    }


}
