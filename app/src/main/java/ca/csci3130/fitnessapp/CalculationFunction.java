package ca.csci3130.fitnessapp;

/**
 * @version iteration 1
 * @auther Amir Farrag, Owen Zhang
 */

public class CalculationFunction {
    /**
     * The CalorieCal function will calculate a numeric double value representing an estimation of how many calories the-
     * -user burns after a workout. It considers the following input parameters:
     *
     * @param (workoutType) integer indicating type of activity, either walking (0) or cycling (1)
     * @param (time)        double indicating amount of time spent working out in hours [0,24]
     * @param (weight)      weight in pounds (positive) range (0,1000]
     * @param (height)      height in inch (positive) range (0,180]
     * @param (distance)    double representing the total walking or cycling distance in metres [0, 300000]
     * @param (o1)          reserved parameter, array
     * @return (double representing amount of burnt calories for the workout)
     */
    public static double CalorieCal(int workoutType, double time, double weight, double height, double distance, Object o1) {

        // Cycling: 14*(14^3*.0083 + 14*.0053*(weight in pounds)) * (time in hours)
        // Walking: (0.035 * 2.20462 * (body weight in pounds)) + ((Velocity in m/s ^ 2) / 39.3701 * (height) * (0.029) * (2.20462) * (weight)

        //workout type is walking
        if (workoutType == 0) {
            return ((.0005* 2.2* weight * distance/time/60 ) * height / 66  + .0035) * time * 60;
        }

        //otherwise, our workout type is cycling
        return 14 * (Math.pow(14.0, 3) * 0.0083 + 14 * 0.0053 * weight) * time;
    }

    /**
     * This function takes in a set of Strings that is directly taken from the UI,
     * Check for the following possible errors:
     * -1 error: empty string
     * -2 error: not a number
     * -4: value out of range
     *
     * @param (testinput) 0: distance; 1: duration of exercise, 2: weight 3: height
     * @param (isKG)      boolean says is the unit in kg/lbs        weightUnit: false-lbs, true-kg
     * @param (isCM)      boolean says is the unit in cm/inch       heightUnit: false-inch, true-cm
     * @return (double array representing amount of corresponding double value described in testinput)
     */
    public static double[] CalorieCalInputTest(String[] testinput, boolean isKG, boolean isCM) {
        // for each for loop, check for one type of error,
        // all errors seperated by comment notice
        double[] ret = new double[testinput.length];

        // error code -1: empty value;
        for (int i = 0; i < testinput.length; i++) {
            if (testinput[i].length() == 0) {
                ret[i] = -1;
            }
        }

        // error code -2: not a number;
        for (int j = 0; j < testinput.length; j++) {
            if (ret[j] == 0) {
                try {
                    Double.valueOf(testinput[j]);
                } catch (NumberFormatException e) {
                    ret[j] = -2;
                }
            }
        }

        // error code -4: value out of range; or give read the value
        if (ret[0] == 0) { // dist: [0,300000]
            if (Double.valueOf(testinput[0]) <= 0 || Double.valueOf(testinput[0]) > 300000) {
                ret[0] = -4;
            } else ret[0] = Double.valueOf(testinput[0]);
        }

        if (ret[1] == 0) { // time: [0,24]
            if (Double.valueOf(testinput[1]) <= 0 || Double.valueOf(testinput[1]) > 24) {
                ret[1] = -4;
            } else ret[1] = Double.valueOf(testinput[1]);
        }

        double temp;
        if (ret[2] == 0) { // weight
            temp = Double.valueOf(testinput[2]);
            if (isKG) { // if input is in kg
                temp *= 2.2046226;
            }
            if (temp <= 0 || temp > 1000) {
                ret[2] = -4;

            } else ret[2] = temp;
        }

        if (ret[3] == 0) { // height
            temp = Double.valueOf(testinput[3]);
            if (isCM) { // if the input is in cm
                temp *= 0.39370079;
            }
            if (temp <= 0 || temp > 180) {
                ret[3] = -4;

            } else ret[3] = temp;
        }

        return ret;
    }
}
