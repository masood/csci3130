package ca.csci3130.fitnessapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.SeekBar;
import android.widget.TextView;

import org.w3c.dom.Text;

public class Goal_weights extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goal_weights);

        // Capture seekbars
        SeekBar seek1 = (SeekBar)findViewById(R.id.weight_seek);
        seek1.setMax(180);

        seek1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                setMin((float)i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }

    private void setMin(float time){
        TextView t = (TextView)findViewById(R.id.weight_time);
        if (time>=0.0 && time<=180.0) {
            t.setText(time + " Minutes");
        }
    }

}
