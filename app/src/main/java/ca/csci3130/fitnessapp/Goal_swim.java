package ca.csci3130.fitnessapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.SeekBar;
import android.widget.TextView;

public class Goal_swim extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goal_swim);
        SeekBar sbar = (SeekBar)findViewById(R.id.swim_bar);
        sbar.setMax(120);

        sbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                time(i); // Update time text
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void time(int time){
        TextView t = (TextView)findViewById(R.id.swim_mins);
        if (time>=0 && time<=120) {
            t.setText(time + " Minutes");
        }
    }
    private TextView getTime() {
        TextView t = (TextView)findViewById(R.id.swim_mins);
        return t;
    }
}
