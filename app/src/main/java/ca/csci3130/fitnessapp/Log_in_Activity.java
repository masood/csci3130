package ca.csci3130.fitnessapp;

import android.app.Activity;
import android.content.Intent;
import android.nfc.Tag;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
public class Log_in_Activity extends AppCompatActivity {

    //private static final String TAG = "CustomAuthActivity";
    // Declare auth
    private FirebaseAuth mAuth;

    private EditText emailLogin;
    private EditText passLogin;

    //private String mCustomToken;
    //private TokenBroadcastReceiver mTokenReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in_);
        // EditTexts
        emailLogin = findViewById(R.id.loginEmail);
        passLogin  = findViewById(R.id.loginPass);

        // Buttons
        Button loginBtn = findViewById(R.id.loginBtn);                 //Log in button
        Button signUpScreenBtn = findViewById(R.id.signUpScreenBtn);   // SignUp screen button

        mAuth = FirebaseAuth.getInstance();

        signUpScreenBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v){
                startActivity(new Intent(Log_in_Activity.this,
                        Sign_up_Activity.class));
            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                (mAuth.signInWithEmailAndPassword(emailLogin.getText().toString(), passLogin.getText().toString()))
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(Log_in_Activity.this,
                                            "Login Successful", Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(Log_in_Activity.this,
                                            signedInPage.class));
                                } else {
                                    Toast.makeText(Log_in_Activity.this,
                                            "Login Failed", Toast.LENGTH_SHORT).show();
                                }
                            }

                        });
            }
        });
    }
}

