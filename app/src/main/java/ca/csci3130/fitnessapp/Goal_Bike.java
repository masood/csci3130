// File by Kenny Power
package ca.csci3130.fitnessapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.SeekBar;
import android.widget.TextView;

public class Goal_Bike extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goal__bike);
        // Capture seekbars
        SeekBar seek1 = (SeekBar)findViewById(R.id.bike_min);
        seek1.setMax(240);
        SeekBar seek2 = (SeekBar)findViewById(R.id.bike_km);
        seek2.setMax(100);

        seek1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                changeMins(i); // Update text
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // Auto generated necessary interface method
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // Auto generated necessary interface method
            }
        });

        seek2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                changeKm((float)i); // Update text
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // Auto generated necessary interface method
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // Auto generated necessary interface method
            }
        });

    }

    private void changeMins(float min) {
        // Method to update text for mins
        TextView t = (TextView)findViewById(R.id.bike_min_view);
        if (min>=0 && min<= 240) {
            t.setText(min + " Minutes");
        }
    }

    private void changeKm(float km) {
        // Method to update text for kms
        TextView t = (TextView)findViewById(R.id.bike_km_view);
        if (km>=0 && km<= 100) {
            t.setText(km + " Kilometers");
        }
    }

}
